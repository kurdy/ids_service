/*
 * This file is part of ids_service
 *
 * ids_service is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ids_service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ids_service.  If not, see <http://www.gnu.org/licenses/>
 */

extern crate ids_service;
extern crate simplelog;

use crate::ids_service::rust_hash::*;
use crate::ids_service::common::*;
use crate::simplelog::*;


fn main() {

    let _ = SimpleLogger::init(LevelFilter::Info, Config::default());

    /*
     * Create an ids service with:
     * Cache size = 100'000
     * hash algo. = rust SipHasher
     * A pool of 20 threads
     */
    let mut ids = IdsService::default();
    ids.start();
    // Optional: Wait cache is filled at 10%
    let _ = ids.filled_at_percent_event(10).recv().is_ok();
    println!("Get an id: {}", ids.get_id());
    println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id"));
    println!("Current numbers of items in cache: {}", ids.get_cache_len());
    // Graceful Shutdown and Cleanup

    ids.set_cache_size(150_000);
    let _ = ids.filled_event().recv().is_ok();
    println!("Current numbers of items in cache: {}", ids.get_cache_len());
    println!("Get an id: {}", ids.get_id());
    ids.stop();


    let mut ids = IdsService::new(100,Some(10 as usize));
    ids.start();
    // Optional: Wait cache is filled
    let _ = ids.filled_event().recv().is_ok();
    for id in ids.take(200) {
        println!("Get an id:{}", id);
    }
}