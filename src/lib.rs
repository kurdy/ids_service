/*
 * This file is part of ids_service
 *
 * ids_service is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ids_service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ids_service.  If not, see <http://www.gnu.org/licenses/>
 */

//! Library that allows generating unique ids. It tried to keep cache filled using background threads
//! to fill it.
//! If the cache is empty (the number of requests is too high) an id is calculated on the fly.
//! This will slow down obtaining the id but there is no error when the cache is empty.
//! To generate unique ids the library uses an array of bytes with values from a random number generator
//! and a timestamp as nanoseconds since January 1, 1970 UTC and finally create an hash from both data's.
//!
//! The library is composed of two modules with similar API but different hash algorithms:
//!
//! ## crypto_hash:
//! The hash algorithm can be one from [crypto::sha3::Sha3Mode](https://docs.rs/rust-crypto/0.2.36/crypto/sha3/enum.Sha3Mode.html).
//!
//! ## rust_hash:
//! The hash algorithm is the [std::collections::hash_map::DefaultHasher] from Rust sdt.
//!
//! According to your requirements, you may choose which modules you need. The negative impact of
//! using crypto_hash  is the performance and resources to run service.
//! Throughput of ids/sec is about half compared to rust DefaultHasher.
//! The indicative throughput with an Intel I7 using sha512 is above 750'000 ids/sec using Rust Hash
//! the throughput is above 900'000 ids/sec.
//!
//! The size of random block is equal to 2 * the size of hash sha512 = 128 bytes.
//! The size of data block to calculate an unique id using sha512 equal 128 bytes random + 8 bytes of
//! timestamp.
//!
//! The id can be encode as:
//! * String encoded hexadecimal lower case
//! * String as Base64
//! * String as Base32
//! * Json
//!
//! # Quick Start
//!
//! ## crypto_hash:
//! ```
//! extern crate ids_service;
//!
//! use crate::ids_service::crypto_hash::*;
//! use crate::ids_service::common::*;
//!
//! fn main() {
//!
//!     /*
//!      * Create an ids service with:
//!      * Cache size = 100'000
//!      * hash algo. = sha256
//!      * A pool of threads = to the cpu numbers
//!      */
//!     let mut ids = IdsService::default();
//!     ids.start();
//!     // Optional: Wait cache is filled at 10%
//!     ids.filled_at_percent_event(10).recv().is_ok();
//!     println!("Get an id: {}", ids.get_id().as_hex());
//!     println!("Get another id: {}", ids.get_id().as_base64());
//!     println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id").as_hex());
//!     println!("Current numbers of items in cache: {}", ids.get_cache_len());
//!     // Graceful Shutdown and Cleanup
//!     ids.stop();
//! }
//! ```
//! Create an hash without caching service
//!
//! ```
//! extern crate ids_service;
//!
//! use crate::ids_service::crypto_hash::*;
//!
//! fn main() {
//!
//!     println!("Create an id sha512 without the service: {}", create_id_as_sha512());
//!     println!("Create an id sha256 without the service: {}", create_id_as_sha256());
//!
//! }
//! ```
//! The lib use simplelog see for configuration:
//! [crates.io Rust Package Registry SimpleLog ](https://crates.io/crates/simplelog).
//!
//! ## rust_hash:
//!
//! ```
//! extern crate ids_service;
//!
//! use crate::ids_service::rust_hash::*;
//! use crate::ids_service::common::*;
//!
//!
//! fn main() {
//!
//!     /*
//!      * Create an ids service with:
//!      * Cache size = 100'000
//!      * hash algo. = rust SipHasher
//!      * A pool of threads = to the cpu numbers
//!      */
//!     let mut ids = IdsService::default();
//!     ids.start();
//!     // Optional: Wait cache is filled at 100%
//!     ids.filled_event().recv().is_ok();
//!     println!("Get an id: {}", ids.get_id());
//!     println!("Get an id: {}", ids.get_id().as_hex());
//!     println!("Get an id: {}", ids.get_id().as_base64());
//!     println!("Get an id: {}", ids.get_id().as_json());
//!     println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id"));
//!     println!("Current numbers of items in cache: {}", ids.get_cache_len());
//!     // Graceful Shutdown and Cleanup
//!     ids.stop();
//! }
//! ```
//! Create an hash without caching service
//!
//! ```
//!
//! extern crate ids_service;
//!
//! use crate::ids_service::rust_hash::*;
//!
//! fn main() {
//!
//!     println!("Create an id without the service: {}", create_id().unwrap());
//!     println!("Create an id without the service: {}", create_id().unwrap());
//!
//! }
//!
//! ```
#[macro_use]
extern crate lazy_static;
extern crate num_cpus;

pub mod crypto_hash;
pub mod rust_hash;


pub use self::crypto_hash::*;
pub use self::rust_hash::*;

/// Module with shared part of lib.
pub mod common {

    // Default number of thread's is equal to the number of cpu's. Tests demonstrated
    // it's good default value
    lazy_static!(
        pub static ref NUMBER_OF_FILL_THREAD: usize = num_cpus::get();
    );

    /// Trait to define possible encoding output.
    pub trait Encode: Sized {
        fn as_hex(&self) -> String {
            unimplemented!()
        }
        fn as_base64(&self) -> String {
            unimplemented!()
        }
        fn as_base64_url(&self) -> String {
            unimplemented!()
        }
        fn as_base32(&self) -> String {
            unimplemented!()
        }

        fn as_json(&self) -> String {
            unimplemented!()
        }
    }

    /// Trait with functions to get an id
    pub trait Uids {
        type Id;

        /// Get an id from cache, if the cache is empty. The function try to create an id on fly.
        fn get_id(&mut self) -> Self::Id {
            unimplemented!()
        }

        /// Get an id from cache. If cache is empty it return None.
        fn get_id_from_cache(&mut self) -> Option<Self::Id>{
            unimplemented!()
        }
    }

    /// Trait with usual functions of service
    pub trait Service {
        fn start(&mut self) {
            unimplemented!()
        }

        fn stop(&mut self) {
            unimplemented!()
        }

        fn restart(&mut self) {
            self.stop();
            self.start();
        }
    }

}