/*
 * This file is part of ids_service
 *
 * ids_service is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ids_service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ids_service.  If not, see <http://www.gnu.org/licenses/>
 */

extern crate ids_service;
extern crate simplelog;

use crate::ids_service::crypto_hash::*;
use crate::ids_service::common::*;
use crate::simplelog::*;
use log::*;

fn main() {

    let _ = SimpleLogger::init(LevelFilter::Info, Config::default());

    /*
     * Create an ids service with:
     * Cache size = 10'000
     * hash algo. = Sha3_256
     * A pool of 100 threads
     */

    let mut ids = IdsService::new(10_000, Sha3Mode::Sha3_256,Some(num_cpus::get() as usize));
    ids.start();

    //Wait the cache is filled at 10%
    let _r=ids.filled_at_percent_event(10).recv().is_ok();
    info!("Current numbers of items in cache: {}", ids.get_cache_len());
    info!("Get an id: {}", ids.get_id().as_hex());
    info!("Get another id: {}", ids.get_id().as_base64());
    info!("Current numbers of items in cache: {}", ids.get_cache_len());

    /*
     * Create a service instance without caching process.
     * Without call of start()
     * Note: You may see messages output log:
     * [INFO] The ids_cache is empty, create on fly an id
     * [INFO] Get an id: 5a1c6815de674c27d4a55f73181b61a88b062c3d1b55e828427e07d0be49557a8fce30dfa19f4f2352eb0044c341cde71ca3960cbf86c075de942677e3b91ed2
     */
    let mut ids2 = IdsService::default();
    info!("Get an id: {}", ids2.get_id().as_hex());

    let mut ids3 = IdsService::default();
    ids3.start();

    //Wait the cache is filled
    let _ = ids3.filled_event().recv().is_ok();
    info!("1) Current numbers of items in cache ids3: {}", ids3.get_cache_len());

    ids3.set_cache_size(150_000);

    //Wait the cache is filled
    let _ = ids3.filled_event().recv().is_ok();

    info!("2) Current numbers of items in cache ids3: {}", ids3.get_cache_len());

    ids.stop();
    ids2.stop();
    ids3.stop();

}