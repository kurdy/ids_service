![Licence](https://img.shields.io/crates/l/ids_service) 
![Version](https://img.shields.io/crates/v/ids_service) 
[![dependency status](https://deps.rs/repo/gitlab/kurdy/ids_service/status.svg)](https://deps.rs/repo/gitlab/kurdy/ids_service) 
![Download](https://img.shields.io/crates/d/ids_service) 
[![pipeline](https://gitlab.com/kurdy/ids_service/badges/master/pipeline.svg)](https://gitlab.com/kurdy/ids_service)
[![docs.rs](https://img.shields.io/docsrs/ids_service)](https://docs.rs/ids_service)

# ids_service 

Library that allows generating unique ids. It tried to keep cache filled using background threads
to fill it.
If the cache is empty (the number of requests is too high) an id is calculated on the fly.
This will slow down obtaining the id but there is no error when the cache is empty.
To generate unique ids the library uses an array of bytes with values from a random number generator
and a timestamp as nanoseconds since January 1, 1970 UTC and finally create an hash from both data's.

The library is composed of two modules with similar API but different hash algorithms:

## crypto_hash:
The hash algorithm can be one from [sha3](https://crates.io/crates/sha3).

## rust_hash:
The hash algorithm is the [std::collections::hash_map::DefaultHasher](https://doc.rust-lang.org/std/collections/hash_map/struct.DefaultHasher.html) from Rust sdt.

---
According to your requirements, you may choose which modules you need. The negative impact of
using crypto_hash  is the performance and resources to run service.
Throughput of ids/sec is about half compared to rust DefaultHasher.
* The indicative throughput with an Intel i7 using sha512 is above 2'000'000 ids/sec 
* The throughput is above 5'000'000 ids/sec using Rust Hash.

The size of random block is equal to 2 * the size of hash sha512 = 128 bytes.
The size of data block to calculate an unique id using sha512 equal 128 bytes random + 8 bytes of
timestamp.

The id can be encode as:
* String encoded hexadecimal lower case
* String as Base64
* String as Base32
* Json


## Quick Start

### Use of default cryptographic hash:

```rust
extern crate ids_service;

use crate::ids_service::crypto_hash::*;
use crate::ids_service::common::*;

fn main() {

    /*
     * Create an ids service with:
     * Cache size = 100'000
     * hash algo. = sha256
     * A pool of 20 threads
     */
    let mut ids = IdsService::default();
    ids.start();
    // Optional: Wait cache is filled at 10%
    let _ = ids.filled_at_percent_event(10).recv().is_ok();
    println!("Get an id: {}", ids.get_id().as_hex());
    println!("Get another id: {}", ids.get_id().as_base64());
    println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id").as_hex());
    println!("Current numbers of items in cache: {}", ids.get_cache_len());
    // Graceful Shutdown and Cleanup
    ids.stop();
}
```
### Use of rust hasher

```rust
extern crate ids_service;
extern crate simplelog;

use crate::ids_service::rust_hash::*;
use crate::ids_service::common::*;
use crate::simplelog::*;
use sha3::Digest;


fn main() {

    let _ = SimpleLogger::init(LevelFilter::Info, Config::default());

    /*
     * Create an ids service with:
     * Cache size = 100'000
     * hash algo. = rust SipHasher
     * A pool of 20 threads
     */
    let mut ids = IdsService::default();
    ids.start();
    // Optional: Wait cache is filled at 10%
    let _ = ids.filled_at_percent_event(10).recv().is_ok();
    println!("Get an id: {}", ids.get_id());
    println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id"));
    println!("Current numbers of items in cache: {}", ids.get_cache_len());
    // Graceful Shutdown and Cleanup
    ids.stop();
}
```


## Run example
Examples ended with __rh_ are using rust hasher

```bash
cargo run --example examples;
cargo run --example examples_rh;
```
```bash
cargo run --example iterator_example;
cargo run --example iterator_example_rh;
```
```bash
cargo run --example quickstart;
cargo run --example quickstart_rh;
```
```bash
cargo run --example quickstart2;
cargo run --example quickstart2_rh;
```
Create ten millions of ids (sha512) and read it all. 
Throughput with i7, linux, 16 CPU's > 2'000'000 ids/s
```bash
cargo run --release --example ten_million;
cargo run --release --example ten_million_rh;
```
## Platforms
The lib is compiled and tested:
1. x86_64 GNU/Linux (dev + tests)
2. FreeBSD (tests)
3. Windows 10 (tests)
4. aarch64 GNU/Linux (test)

