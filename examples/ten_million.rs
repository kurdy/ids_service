/*
 * This file is part of ids_service
 *
 * ids_service is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ids_service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ids_service.  If not, see <http://www.gnu.org/licenses/>
 */

extern crate ids_service;
extern crate simplelog;

use crate::ids_service::crypto_hash::*;
use crate::ids_service::common::*;
use crate::simplelog::*;
use std::time::Instant;
use log::*;

fn main() {

    const NUMBER_OF_ITEMS : usize = 10_000_000;

    info!("Number of CPU's={}",num_cpus::get());

    let _ = SimpleLogger::init(LevelFilter::Info, Config::default());
    let start = Instant::now();
    let mut ids = IdsService::new(NUMBER_OF_ITEMS/10, Sha3Mode::Sha3_512,Some(num_cpus::get() * 4 as usize));
    ids.start();
    let _ = ids.filled_at_percent_event(10).recv().is_ok();
    //info!("Cache filled. {} in {:?} ",ids.get_cache_len(), start.elapsed());

    for i in 0..NUMBER_OF_ITEMS {
        let id : String = ids.get_id().as_hex();
        if id.len() != 128 {
            warn!("Id not valid. {}", id);
        }
        if i%(NUMBER_OF_ITEMS/100) == 0 {
            info!("Number of items get {}, cache size {}, in {:?}", i,ids.get_cache_len(),start.elapsed());
        }
    }
    let duration = start.elapsed().as_secs();
    info!("Cache size {}. Operation duration: {}s, throughput {} ids/s ",ids.get_cache_len(),duration, NUMBER_OF_ITEMS as u64/duration);
    ids.stop();
}