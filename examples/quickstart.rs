/*
 * This file is part of ids_service
 *
 * ids_service is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ids_service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ids_service.  If not, see <http://www.gnu.org/licenses/>
 */

extern crate ids_service;

use crate::ids_service::crypto_hash::*;
use crate::ids_service::common::*;

fn main() {

    /*
     * Create an ids service with:
     * Cache size = 100'000
     * hash algo. = sha256
     * A pool of 20 threads
     */
    let mut ids = IdsService::default();
    ids.start();
    // Optional: Wait cache is filled at 10%
    let _ = ids.filled_at_percent_event(10).recv().is_ok();
    println!("Get an id: {}", ids.get_id().as_hex());
    println!("Get another id: {}", ids.get_id().as_base64());
    println!("Get an id from cache: {}", ids.get_id_from_cache().expect("Expect an id").as_hex());
    println!("Current numbers of items in cache: {}", ids.get_cache_len());
    // Graceful Shutdown and Cleanup
    ids.stop();
}